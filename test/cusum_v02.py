# TurboCython test suite
# (C) Roman Pulgrabja, Marek Legris, 2019

import pandas as pd
import numpy as np


# Original Author: Artur Andrzejak
# For the ease of use some changes need to be done on this example
def get_CUSUM_v01(inseries: pd.Series, posThreshold, negThreshold = None, applyDiff = True):

    negThreshold : float = negThreshold if negThreshold is not None else posThreshold

    posEventsList, negEventsList = [], []
    posSum, negSum = 0.0, 0.0
    # problem: no memview equivalent for diff -> cant use memview
    diff = inseries.diff() if applyDiff else inseries
    for i in range(1, len(diff)):
        # type declarations in for loops get ignored for now
        posSum = max(0, posSum + diff.iloc[i])
        negSum = min(0, negSum + diff.iloc[i])

        if negSum < -negThreshold:
            negSum = 0; negEventsList.append(i)
        elif posSum > posThreshold:
            posSum = 0; posEventsList.append(i)

    # Subset the original index of the series, and return them
    negEventIndex  = inseries.index[negEventsList]
    posEventIndex  = inseries.index[posEventsList]
    return posEventIndex, negEventIndex, posEventsList, negEventsList