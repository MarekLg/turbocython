import cython
import pandas as pd

def error(input: pd.Series):
    import sys
    sys.path.insert(0, '/Users/marek/Documents/Informatik/puremv4cython/TurboCython')
    from TurboCython import helpers
    cdef double[:] input_memview = input.values
    return 0

cpdef int no_error(int[:] input):
    return 0