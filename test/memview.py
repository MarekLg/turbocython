# TurboCython test suite
# (C) Roman Pulgrabja, Marek Legris, 2019

import numpy as np

narr = np.arange(27, dtype=np.dtype("i")).reshape((3, 3, 3))
narr_view = narr # type: int[:, :, :]
print("NumPy sum of the NumPy array before assignments: %s" % narr.sum())
narr_view[:, :, :] = 3
print("NumPy sum of NumPy array after assignments: %s" % narr.sum())

def sum3d(arr: int[:, :, :]) -> int:
    i: int
    j: int
    k: int
    total: int = 0
    I = arr.shape[0]
    J = arr.shape[1]
    K = arr.shape[2]
    for i in range(I):
        for j in range(J):
            for k in range(K):
                total += arr[i, j, k]
    return total
print("Memoryview sum of NumPy array is %s" % sum3d(narr))
print("Memoryview sum of NumPy memoryview is %s" % sum3d(narr_view))