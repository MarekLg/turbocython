# TurboCython test suite
# (C) Roman Pulgrabja, Marek Legris, 2019

import pandas as pd


def error(input: pd.Series) -> int:
    return 0


def no_error(input: int[:]) -> int:
    return 0