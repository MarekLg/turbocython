import cython
import pandas as pd
import numpy as np

def get_CUSUM_v01(inseries: pd.Series, posThreshold, negThreshold=None, applyDiff=True):
    import sys
    sys.path.insert(0, '/Users/marek/Documents/Informatik/puremv4cython/TurboCython')
    from TurboCython import helpers
    cdef double[:] inseries_memview = inseries.values
    cdef float negThreshold = (negThreshold if (negThreshold is not None) else posThreshold)
    (posEventsList, negEventsList) = ([], [])
    (posSum, negSum) = (0.0, 0.0)
    diff = (helpers.diff(inseries_memview) if applyDiff else inseries_memview)
    for i in range(1, len(diff)):
        posSum = max(0, (posSum + diff[i]))
        negSum = min(0, (negSum + diff[i]))
        if (negSum < (- negThreshold)):
            negSum = 0
            negEventsList.append(i)
        elif (posSum > posThreshold):
            posSum = 0
            posEventsList.append(i)
    negEventIndex = np.array(negEventsList)
    posEventIndex = np.array(posEventsList)
    return (posEventIndex, negEventIndex, posEventsList, negEventsList)