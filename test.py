# TurboCython test suite
# (C) Roman Pulgrabja, Marek Legris, 2019

import unittest
import os
import parsing

class TurboTest(unittest.TestCase):
    def basic_test(self, filename):
        dirpath = os.path.dirname(__file__)
        with open(os.path.join(dirpath, "test/{0}.pyx".format(filename))) as f:
            pyx = f.read()
        with open(os.path.join(dirpath, "test/{0}.py".format(filename))) as f:
            src = f.read()
            parsed = parsing.parse_pyx(src)

        pyxlines = pyx.splitlines()
        parsedlines = parsed.splitlines()

        for i in range(len(pyxlines)):
            self.assertEqual(pyxlines[i], parsedlines[i])

    def test_memview(self):
        self.basic_test("memview")
    def test_pandas_error(self):
        self.basic_test("pandas_error")
    def test_cusum(self):
        self.basic_test("cusum_v02")


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TurboTest)
    unittest.TextTestRunner(verbosity=2).run(suite)