from distutils.core import setup
from Cython.Build import cythonize
import numpy

# cmd line pars:  build_ext --inplace
setup(
    ext_modules = cythonize(["helpers.pyx"], build_dir = "../"),
    include_dirs=[numpy.get_include()]
)