# TurboCython
# Original code by simonpercivall, found at https://github.com/simonpercivall/astunparse
# (C) Roman Pulgrabja, Marek Legris, 2019

import six
import typed_ast.ast3 as ast
import sys
import helpers


INFSTR = "1e" + repr(sys.float_info.max_10_exp + 1)


def interleave(inter, f, seq, flags):
    """Call f on each item in seq, calling inter() in between with flags[:].
    """
    seq = iter(seq)
    try:
        f(next(seq), flags[:])
    except StopIteration:
        pass
    else:
        for x in seq:
            inter()
            f(x, flags[:])


class Unparse:
    """Methods in this class recursively traverse an AST and
    output source code for the abstract syntax; original formatting
    is disregarded. """
    
    def __init__(self, tree, packagePath, *flags):
        """Unparser(tree) -> str.
        Print the source for tree to string (pyx source)."""
        self.packagePath = packagePath
        self.imports = {}
        self.src = ""
        self.future_imports = []
        self._indent = 0
        self.dispatch(tree, [flag for flag in flags])

    def fill(self, text = ""):
        "Indent a piece of text, according to the current indentation level"
        self.src += "\n"+"    "*self._indent + text

    def write(self, text):
        "Append a piece of text to the current line."
        self.src += six.text_type(text)

    def enter(self):
        "Print ':', and increase the indentation."
        self.write(":")
        self._indent += 1

    def leave(self):
        "Decrease the indentation level."
        self._indent -= 1

    def dispatch(self, tree, flags):
        "Dispatcher function, dispatching tree type T to method _T."

        if isinstance(tree, list):
            for t in tree:
                self.dispatch(t, flags[:])
            return
        meth = getattr(self, "_"+tree.__class__.__name__)
        meth(tree, flags[:])

    def checkFunction(self, tree, pandas_vars):
        """Check if function contains pandas function not present in helpers.

        Args:
            tree: The tree that is to be checked, presumably a FunctionDef node

        Returns:
            A bool that is True if every function can be replaced with one from helpers
        """

        for node in ast.walk(tree):
            if isinstance(node, ast.Attribute):
                if isinstance(node.value, ast.Name):
                    if node.value.id in pandas_vars:
                        # pandas call detected

                        if not helpers.containsMember(node.attr):
                            return False

        # no problem found
        return True


    ############### Unparsing methods ######################
    # There should be one method per concrete grammar type #
    # Constructors should be grouped by sum type. Ideally, #
    # this would follow the order in the grammar, but      #
    # currently doesn't.                                   #
    ########################################################

    def _Module(self, tree, flags):
        for stmt in tree.body:
            self.dispatch(stmt, flags[:])

    def _Interactive(self, tree, flags):
        for stmt in tree.body:
            self.dispatch(stmt, flags[:])

    def _Expression(self, tree, flags):
        self.dispatch(tree.body, flags[:])

    # stmt
    def _Expr(self, tree, flags):
        self.fill()
        self.dispatch(tree.value, flags[:])

    def _Import(self, t, flags):
        # Add import to Dict
        for alias in t.names:
            self.imports[alias.name] = alias.asname if alias.asname != None else alias.name

        self.fill("import ")
        interleave(lambda: self.write(", "), self.dispatch, t.names, flags[:])

    def _ImportFrom(self, t, flags):
        # A from __future__ import may affect unparsing, so record it.
        if t.module and t.module == '__future__':
            self.future_imports.extend(n.name for n in t.names)

        self.fill("from ")
        self.write("." * t.level)
        if t.module:
            self.write(t.module)
        self.write(" import ")
        interleave(lambda: self.write(", "), self.dispatch, t.names, flags[:])

    def _Assign(self, t, flags):
        self.fill()
        done = False

        # check for PEP 484 type comment
        if t.type_comment != None:
            self.write("cdef " + t.type_comment + " ")
            
        # dispatch targets
        for target in t.targets:
            self.dispatch(target, flags[:])
            self.write(" = ")

        # check for memoryview() call
        if isinstance(t.value, ast.Call):
            if isinstance(t.value.func, ast.Name):
                if t.value.func.id is "memoryview":    
                    self.dispatch(t.value.args, flags[:])
                    done = True

        if not done:
            self.dispatch(t.value, flags[:])
                

    def _AugAssign(self, t, flags):
        self.fill()
        self.dispatch(t.target, flags[:])
        self.write(" "+self.binop[t.op.__class__.__name__]+"= ")
        self.dispatch(t.value, flags[:])

    def _AnnAssign(self, t, flags):
        self.fill()
        #if not t.simple:
        #    self.write("(")
        #if not t.simple:
        #    self.write(")")
        self.write("cdef ")
        self.dispatch(t.annotation, flags[:])
        self.write(" ")
        self.dispatch(t.target, flags[:])
        if t.value:
            self.write(" = ")
            self.dispatch(t.value, flags[:])

    def _Return(self, t, flags):
        self.fill("return")
        if t.value:
            self.write(" ")
            self.dispatch(t.value, flags[:])

    def _Pass(self, t, flags):
        self.fill("pass")

    def _Break(self, t, flags):
        self.fill("break")

    def _Continue(self, t, flags):
        self.fill("continue")

    def _Delete(self, t, flags):
        self.fill("del ")
        interleave(lambda: self.write(", "), self.dispatch, t.targets, flags[:])

    def _Assert(self, t, flags):
        self.fill("assert ")
        self.dispatch(t.test, flags[:])
        if t.msg:
            self.write(", ")
            self.dispatch(t.msg, flags[:])

    def _Exec(self, t, flags):
        self.fill("exec ")
        self.dispatch(t.body, flags[:])
        if t.globals:
            self.write(" in ")
            self.dispatch(t.globals, flags[:])
        if t.locals:
            self.write(", ")
            self.dispatch(t.locals, flags[:])

    def _Print(self, t, flags):
        self.fill("print ")
        do_comma = False
        if t.dest:
            self.write(">>")
            self.dispatch(t.dest, flags[:])
            do_comma = True
        for e in t.values:
            if do_comma:self.write(", ")
            else:do_comma=True
            self.dispatch(e, flags[:])
        if not t.nl:
            self.write(",")

    def _Global(self, t, flags):
        self.fill("global ")
        interleave(lambda: self.write(", "), self.write, t.names, flags[:])

    def _Nonlocal(self, t, flags):
        self.fill("nonlocal ")
        interleave(lambda: self.write(", "), self.write, t.names, flags[:])

    def _Yield(self, t, flags):
        self.write("(")
        self.write("yield")
        if t.value:
            self.write(" ")
            self.dispatch(t.value, flags[:])
        self.write(")")

    def _YieldFrom(self, t, flags):
        self.write("(")
        self.write("yield from")
        if t.value:
            self.write(" ")
            self.dispatch(t.value, flags[:])
        self.write(")")

    def _Raise(self, t, flags):
        self.fill("raise")
        if six.PY3:
            if not t.exc:
                assert not t.cause
                return
            self.write(" ")
            self.dispatch(t.exc, flags[:])
            if t.cause:
                self.write(" from ")
                self.dispatch(t.cause, flags[:])
        else:
            self.write(" ")
            if t.type:
                self.dispatch(t.type, flags[:])
            if t.inst:
                self.write(", ")
                self.dispatch(t.inst, flags[:])
            if t.tback:
                self.write(", ")
                self.dispatch(t.tback, flags[:])

    def _Try(self, t, flags):
        self.fill("try")
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()
        for ex in t.handlers:
            self.dispatch(ex, flags[:])
        if t.orelse:
            self.fill("else")
            self.enter()
            self.dispatch(t.orelse, flags[:])
            self.leave()
        if t.finalbody:
            self.fill("finally")
            self.enter()
            self.dispatch(t.finalbody, flags[:])
            self.leave()

    def _TryExcept(self, t, flags):
        self.fill("try")
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()

        for ex in t.handlers:
            self.dispatch(ex, flags[:])
        if t.orelse:
            self.fill("else")
            self.enter()
            self.dispatch(t.orelse, flags[:])
            self.leave()

    def _TryFinally(self, t, flags):
        if len(t.body) == 1 and isinstance(t.body[0], ast.TryExcept):
            # try-except-finally
            self.dispatch(t.body, flags[:])
        else:
            self.fill("try")
            self.enter()
            self.dispatch(t.body, flags[:])
            self.leave()

        self.fill("finally")
        self.enter()
        self.dispatch(t.finalbody, flags[:])
        self.leave()

    def _ExceptHandler(self, t, flags):
        self.fill("except")
        if t.type:
            self.write(" ")
            self.dispatch(t.type, flags[:])
        if t.name:
            self.write(" as ")
            if six.PY3:
                self.write(t.name)
            else:
                self.dispatch(t.name, flags[:])
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()

    def _ClassDef(self, t, flags):
        self.write("\n")
        for deco in t.decorator_list:
            self.fill("@")
            self.dispatch(deco, flags[:])
        self.fill("cdef class "+t.name)
        if six.PY3:
            self.write("(")
            comma = False
            for e in t.bases:
                if comma: self.write(", ")
                else: comma = True
                self.dispatch(e, flags[:])
            for e in t.keywords:
                if comma: self.write(", ")
                else: comma = True
                self.dispatch(e, flags[:])
            if sys.version_info[:2] < (3, 5):
                if t.starargs:
                    if comma: self.write(", ")
                    else: comma = True
                    self.write("*")
                    self.dispatch(t.starargs, flags[:])
                if t.kwargs:
                    if comma: self.write(", ")
                    else: comma = True
                    self.write("**")
                    self.dispatch(t.kwargs, flags[:])
            self.write(")")
        elif t.bases:
                self.write("(")
                for a in t.bases:
                    self.dispatch(a, flags[:])
                    self.write(", ")
                self.write(")")
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()

    def _generic_FunctionDef(self, t, flags, async_=False):
        # check for pandas parameters
        pandas_params = []
        if 'pandas' in self.imports:
            for arg in t.args.args:
                if arg.annotation:
                    if isinstance(arg.annotation, ast.Attribute):
                        if isinstance(arg.annotation.value, ast.Name):
                            if arg.annotation.value.id is self.imports['pandas']:
                                # add found pandas parameter to list
                                pandas_params.append(arg.arg)
        
        if pandas_params and self.checkFunction(t, pandas_params):
            # every pandas function used can be exchanged
            flags.append("override_pandas")
            flags += pandas_params

        self.write("\n")
        returnStr = ""
        for deco in t.decorator_list:
            self.fill("@")
            self.dispatch(deco, flags[:])
        
        # if pandas parameters are used, use default python function definition
        if pandas_params:
            self.fill(("async " if async_ else "") + "def ")
        else:
            self.fill(("async " if async_ else "") + "cpdef ")
            if getattr(t, "returns", False):
                self.dispatch(t.returns, flags[:])
                self.write(" ")
        self.write(t.name + "(")
        self.dispatch(t.args, flags[:])
        self.write(")")
        self.enter()

        if "override_pandas" in flags:
            # import helpers
            self.fill("import sys")
            self.fill("sys.path.insert(0, '{0}')".format(self.packagePath))
            self.fill("from TurboCython import helpers")

            # add pandas variables after function definition
            for name in pandas_params:
                self.fill("cdef double[:] {0}_memview = {0}.values".format(name))

        self.dispatch(t.body, flags[:])
        self.leave()

    def _FunctionDef(self, t, flags):
        self._generic_FunctionDef(t, flags[:])

    def _AsyncFunctionDef(self, t, flags):
        self._generic_FunctionDef(t, flags[:], async_=True)

    def _generic_For(self, t, flags, async_=False):
        self.fill("async for " if async_ else "for ")
        self.dispatch(t.target, flags[:])
        self.write(" in ")
        self.dispatch(t.iter, flags[:])
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()
        if t.orelse:
            self.fill("else")
            self.enter()
            self.dispatch(t.orelse, flags[:])
            self.leave()

    def _For(self, t, flags):
        self._generic_For(t, flags[:])

    def _AsyncFor(self, t, flags):
        self._generic_For(t, flags[:], async_=True)

    def _If(self, t, flags):
        self.fill("if ")
        self.dispatch(t.test, flags[:])
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()
        # collapse nested ifs into equivalent elifs.
        while (t.orelse and len(t.orelse) == 1 and
               isinstance(t.orelse[0], ast.If)):
            t = t.orelse[0]
            self.fill("elif ")
            self.dispatch(t.test, flags[:])
            self.enter()
            self.dispatch(t.body, flags[:])
            self.leave()
        # final else
        if t.orelse:
            self.fill("else")
            self.enter()
            self.dispatch(t.orelse, flags[:])
            self.leave()

    def _While(self, t, flags):
        self.fill("while ")
        self.dispatch(t.test, flags[:])
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()
        if t.orelse:
            self.fill("else")
            self.enter()
            self.dispatch(t.orelse, flags[:])
            self.leave()

    def _generic_With(self, t, flags, async_=False):
        self.fill("async with " if async_ else "with ")
        if hasattr(t, 'items'):
            interleave(lambda: self.write(", "), self.dispatch, t.items, flags[:])
        else:
            self.dispatch(t.context_expr, flags[:])
            if t.optional_vars:
                self.write(" as ")
                self.dispatch(t.optional_vars, flags[:])
        self.enter()
        self.dispatch(t.body, flags[:])
        self.leave()

    def _With(self, t, flags):
        self._generic_With(t, flags[:])

    def _AsyncWith(self, t, flags):
        self._generic_With(t, flags[:], async_=True)

    # expr
    def _Bytes(self, t, flags):
        self.write(repr(t.s))

    def _Str(self, tree, flags):
        if six.PY3:
            self.write(repr(tree.s))
        else:
            # if from __future__ import unicode_literals is in effect,
            # then we want to output string literals using a 'b' prefix
            # and unicode literals with no prefix.
            if "unicode_literals" not in self.future_imports:
                self.write(repr(tree.s))
            elif isinstance(tree.s, str):
                self.write("b" + repr(tree.s))
            elif isinstance(tree.s, unicode):
                self.write(repr(tree.s).lstrip("u"))
            else:
                assert False, "shouldn't get here"

    def _JoinedStr(self, t, flags):
        # JoinedStr(expr* values)
        self.write("f")
        string = StringIO()
        self._fstring_JoinedStr(t, string.write)
        # Deviation from `unparse.py`: Try to find an unused quote.
        # This change is made to handle _very_ complex f-strings.
        v = string.getvalue()
        if '\n' in v or '\r' in v:
            quote_types = ["'''", '"""']
        else:
            quote_types = ["'", '"', '"""', "'''"]
        for quote_type in quote_types:
            if quote_type not in v:
                v = "{quote_type}{v}{quote_type}".format(quote_type=quote_type, v=v)
                break
        else:
            v = repr(v)
        self.write(v)

    def _FormattedValue(self, t, flags):
        # FormattedValue(expr value, int? conversion, expr? format_spec)
        self.write("f")
        string = StringIO()
        self._fstring_JoinedStr(t, string.write)
        self.write(repr(string.getvalue()))

    def _fstring_JoinedStr(self, t, write):
        for value in t.values:
            meth = getattr(self, "_fstring_" + type(value).__name__)
            meth(value, write)

    def _fstring_Str(self, t, write):
        value = t.s.replace("{", "{{").replace("}", "}}")
        write(value)

    def _fstring_Constant(self, t, write):
        assert isinstance(t.value, str)
        value = t.value.replace("{", "{{").replace("}", "}}")
        write(value)

    def _fstring_FormattedValue(self, t, write):
        write("{")
        expr = StringIO()
        Unparser(t.value, expr)
        expr = expr.getvalue().rstrip("\n")
        if expr.startswith("{"):
            write(" ")  # Separate pair of opening brackets as "{ {"
        write(expr)
        if t.conversion != -1:
            conversion = chr(t.conversion)
            assert conversion in "sra"
            write("!{conversion}".format(conversion=conversion))
        if t.format_spec:
            write(":")
            meth = getattr(self, "_fstring_" + type(t.format_spec).__name__)
            meth(t.format_spec, write)
        write("}")

    def _Name(self, t, flags):
        self.write(t.id)
                
        if "override_pandas" in flags:
            if t.id in flags:
                self.write("_memview")

    def _NameConstant(self, t, flags):
        self.write(repr(t.value))

    def _Repr(self, t, flags):
        self.write("`")
        self.dispatch(t.value, flags[:])
        self.write("`")

    def _write_constant(self, value):
        if isinstance(value, (float, complex)):
            # Substitute overflowing decimal literal for AST infinities.
            self.write(repr(value).replace("inf", INFSTR))
        else:
            self.write(repr(value))

    def _Constant(self, t, flags):
        value = t.value
        if isinstance(value, tuple):
            self.write("(")
            if len(value) == 1:
                self._write_constant(value[0])
                self.write(",")
            else:
                interleave(lambda: self.write(", "), self._write_constant, value)
            self.write(")")
        elif value is Ellipsis: # instead of `...` for Py2 compatibility
            self.write("...")
        else:
            self._write_constant(t.value)

    def _Num(self, t, flags):
        repr_n = repr(t.n)
        if six.PY3:
            self.write(repr_n.replace("inf", INFSTR))
        else:
            # Parenthesize negative numbers, to avoid turning (-1)**2 into -1**2.
            if repr_n.startswith("-"):
                self.write("(")
            if "inf" in repr_n and repr_n.endswith("*j"):
                repr_n = repr_n.replace("*j", "j")
            # Substitute overflowing decimal literal for AST infinities.
            self.write(repr_n.replace("inf", INFSTR))
            if repr_n.startswith("-"):
                self.write(")")

    def _List(self, t, flags):
        self.write("[")
        interleave(lambda: self.write(", "), self.dispatch, t.elts, flags[:])
        self.write("]")

    def _ListComp(self, t, flags):
        self.write("[")
        self.dispatch(t.elt, flags[:])
        for gen in t.generators:
            self.dispatch(gen, flags[:])
        self.write("]")

    def _GeneratorExp(self, t, flags):
        self.write("(")
        self.dispatch(t.elt, flags[:])
        for gen in t.generators:
            self.dispatch(gen, flags[:])
        self.write(")")

    def _SetComp(self, t, flags):
        self.write("{")
        self.dispatch(t.elt, flags[:])
        for gen in t.generators:
            self.dispatch(gen, flags[:])
        self.write("}")

    def _DictComp(self, t, flags):
        self.write("{")
        self.dispatch(t.key, flags[:])
        self.write(": ")
        self.dispatch(t.value, flags[:])
        for gen in t.generators:
            self.dispatch(gen, flags[:])
        self.write("}")

    def _comprehension(self, t, flags):
        if getattr(t, 'is_async', False):
            self.write(" async")
        self.write(" for ")
        self.dispatch(t.target, flags[:])
        self.write(" in ")
        self.dispatch(t.iter, flags[:])
        for if_clause in t.ifs:
            self.write(" if ")
            self.dispatch(if_clause, flags[:])

    def _IfExp(self, t, flags):
        self.write("(")
        self.dispatch(t.body, flags[:])
        self.write(" if ")
        self.dispatch(t.test, flags[:])
        self.write(" else ")
        self.dispatch(t.orelse, flags[:])
        self.write(")")

    def _Set(self, t, flags):
        assert(t.elts) # should be at least one element
        self.write("{")
        interleave(lambda: self.write(", "), self.dispatch, t.elts, flags[:])
        self.write("}")

    def _Dict(self, t, flags):
        self.write("{")
        def write_pair(pair):
            (k, v) = pair
            if k is None:
                self.write('**')
                self.dispatch(v, flags[:])
            else:
                self.dispatch(k, flags[:])
                self.write(": ")
                self.dispatch(v, flags[:])
            self.write(",")
        self._indent +=1
        self.fill("")
        interleave(lambda: self.fill(""), write_pair, zip(t.keys, t.values))
        self._indent -=1
        self.fill("}")

    def _Tuple(self, t, flags):
        self.write("(")
        if len(t.elts) == 1:
            (elt,) = t.elts
            self.dispatch(elt, flags[:])
            self.write(",")
        else:
            interleave(lambda: self.write(", "), self.dispatch, t.elts, flags[:])
        self.write(")")

    unop = {"Invert":"~", "Not": "not", "UAdd":"+", "USub":"-"}
    def _UnaryOp(self, t, flags):
        self.write("(")
        self.write(self.unop[t.op.__class__.__name__])
        self.write(" ")
        if six.PY2 and isinstance(t.op, ast.USub) and isinstance(t.operand, ast.Num):
            # If we're applying unary minus to a number, parenthesize the number.
            # This is necessary: -2147483648 is different from -(2147483648) on
            # a 32-bit machine (the first is an int, the second a long), and
            # -7j is different from -(7j).  (The first has real part 0.0, the second
            # has real part -0.0.)
            self.write("(")
            self.dispatch(t.operand, flags[:])
            self.write(")")
        else:
            self.dispatch(t.operand, flags[:])
        self.write(")")

    binop = { "Add":"+", "Sub":"-", "Mult":"*", "Div":"/", "Mod":"%",
                    "LShift":"<<", "RShift":">>", "BitOr":"|", "BitXor":"^", "BitAnd":"&",
                    "FloorDiv":"//", "Pow": "**",
                    "MatMult":"@"}
    def _BinOp(self, t, flags):
        self.write("(")
        self.dispatch(t.left, flags[:])
        self.write(" " + self.binop[t.op.__class__.__name__] + " ")
        self.dispatch(t.right, flags[:])
        self.write(")")

    cmpops = {"Eq":"==", "NotEq":"!=", "Lt":"<", "LtE":"<=", "Gt":">", "GtE":">=",
                        "Is":"is", "IsNot":"is not", "In":"in", "NotIn":"not in"}
    def _Compare(self, t, flags):
        self.write("(")
        self.dispatch(t.left, flags[:])
        for o, e in zip(t.ops, t.comparators):
            self.write(" " + self.cmpops[o.__class__.__name__] + " ")
            self.dispatch(e, flags[:])
        self.write(")")

    boolops = {ast.And: 'and', ast.Or: 'or'}
    def _BoolOp(self, t, flags):
        self.write("(")
        s = " %s " % self.boolops[t.op.__class__]
        interleave(lambda: self.write(s), self.dispatch, t.values, flags[:])
        self.write(")")

    def _Attribute(self, t, flags):
        self.dispatch(t.value, flags[:])
        # Special case: 3.__abs__() is a syntax error, so if t.value
        # is an integer literal then we need to either parenthesize
        # it or add an extra space to get 3 .__abs__().
        if isinstance(t.value, ast.Num) and isinstance(t.value.n, int):
            self.write(" ")
        self.write(".")
        self.write(t.attr)

    def _Call(self, t, flags):
        done = False
        # check for pandas functions to replace
        if "override_pandas" in flags:
            if isinstance(t.func, ast.Attribute):
                if isinstance(t.func.value, ast.Name):
                    if t.func.value.id in flags:
                        # replace pandas function with helpers function
                        # TODO: this only allows for pandas functions with empty parameters
                        self.write("helpers.{0}(".format(t.func.attr))
                        self.dispatch(t.func.value, flags[:])
                        self.write(")")
                        done = True

        if not done:
            self.dispatch(t.func, flags[:])
            self.write("(")
            comma = False
            for e in t.args:
                if comma: self.write(", ")
                else: comma = True
                self.dispatch(e, flags[:])
            for e in t.keywords:
                if comma: self.write(", ")
                else: comma = True
                self.dispatch(e, flags[:])
            if sys.version_info[:2] < (3, 5):
                if t.starargs:
                    if comma: self.write(", ")
                    else: comma = True
                    self.write("*")
                    self.dispatch(t.starargs, flags[:])
                if t.kwargs:
                    if comma: self.write(", ")
                    else: comma = True
                    self.write("**")
                    self.dispatch(t.kwargs, flags[:])
            self.write(")")

    def _Subscript(self, t, flags):
        done = False
        # check for pandas access
        # TODO: make modular maybe?
        if isinstance(t.value, ast.Attribute):
            if isinstance(t.value.value, ast.Name):
                if t.value.attr is "iloc":
                    self.dispatch(t.value.value, flags[:])
                    self.write("[")
                    self.dispatch(t.slice, flags[:])
                    self.write("]")
                    done = True
                elif t.value.attr is "index":
                    self.write("numpy.array(")
                    self.dispatch(t.slice, flags[:])
                    self.write(")")
                    done = True
                    

        if not done:
            self.dispatch(t.value, flags[:])
            self.write("[")
            self.dispatch(t.slice, flags[:])
            self.write("]")

    def _Starred(self, t, flags):
        self.write("*")
        self.dispatch(t.value, flags[:])

    # slice
    def _Ellipsis(self, t, flags):
        self.write("...")

    def _Index(self, t, flags):
        self.dispatch(t.value, flags[:])

    def _Slice(self, t, flags):
        if t.lower:
            self.dispatch(t.lower, flags[:])
        self.write(":")
        if t.upper:
            self.dispatch(t.upper, flags[:])
        if t.step:
            self.write(":")
            self.dispatch(t.step, flags[:])

    def _ExtSlice(self, t, flags):
        interleave(lambda: self.write(', '), self.dispatch, t.dims, flags[:])

    # argument
    def _arg(self, t, flags):
        if "override_pandas" in flags:
            self.write(t.arg)
            if t.annotation:
                self.write(': ')
                self.dispatch(t.annotation, flags[:])
        else:
            if t.annotation:
                self.dispatch(t.annotation, flags[:])
                self.write(" ")
            self.write(t.arg)

    # others
    def _arguments(self, t, flags):
        first = True
        # normal arguments
        defaults = [None] * (len(t.args) - len(t.defaults)) + t.defaults
        for a,d in zip(t.args, defaults):
            if first:first = False
            else: self.write(", ")
            self.dispatch(a, flags[:])
            if d:
                self.write("=")
                self.dispatch(d, flags[:])

        # varargs, or bare '*' if no varargs but keyword-only arguments present
        if t.vararg or getattr(t, "kwonlyargs", False):
            if first: first = False
            else: self.write(", ")
            self.write("*")
            if t.vararg:
                if hasattr(t.vararg, 'arg'):
                    self.write(t.vararg.arg)
                    if t.vararg.annotation:
                        self.write(": ")
                        self.dispatch(t.vararg.annotation, flags[:])
                else:
                    self.write(t.vararg)
                    if getattr(t, 'varargannotation', None):
                        self.write(": ")
                        self.dispatch(t.varargannotation, flags[:])

        # keyword-only arguments
        if getattr(t, "kwonlyargs", False):
            for a, d in zip(t.kwonlyargs, t.kw_defaults):
                if first:first = False
                else: self.write(", ")
                self.dispatch(a, flags[:]),
                if d:
                    self.write("=")
                    self.dispatch(d, flags[:])

        # kwargs
        if t.kwarg:
            if first:first = False
            else: self.write(", ")
            if hasattr(t.kwarg, 'arg'):
                self.write("**"+t.kwarg.arg)
                if t.kwarg.annotation:
                    self.write(": ")
                    self.dispatch(t.kwarg.annotation, flags[:])
            else:
                self.write("**"+t.kwarg)
                if getattr(t, 'kwargannotation', None):
                    self.write(": ")
                    self.dispatch(t.kwargannotation, flags[:])

    def _keyword(self, t, flags):
        if t.arg is None:
            # starting from Python 3.5 this denotes a kwargs part of the invocation
            self.write("**")
        else:
            self.write(t.arg)
            self.write("=")
        self.dispatch(t.value, flags[:])

    def _Lambda(self, t, flags):
        self.write("(")
        self.write("lambda ")
        self.dispatch(t.args, flags[:])
        self.write(": ")
        self.dispatch(t.body, flags[:])
        self.write(")")

    def _alias(self, t, flags):
        self.write(t.name)
        if t.asname:
            self.write(" as "+t.asname)

    def _withitem(self, t, flags):
        self.dispatch(t.context_expr, flags[:])
        if t.optional_vars:
            self.write(" as ")
            self.dispatch(t.optional_vars, flags[:])

    def _Await(self, t, flags):
        self.write("(")
        self.write("await")
        if t.value:
            self.write(" ")
            self.dispatch(t.value, flags[:])
        self.write(")")
