# TurboCython test suite
# (C) Roman Pulgrabja, Marek Legris, 2019

import cython
import pandas as pd
import numpy as np
from TurboCython import helpers


# this is how the pyx should look like after parsing

# lets first try this with a 'def' function. Hence pd.series will be replaced inside the function. In this first try
# we will only use numpy to replace pandas. Later we will also apply memoryviews
def get_CUSUM_v01_np_inside(inseries: pd.Series, posThreshold, negThreshold=None, applyDiff=True):
    #extract data from pandas series
    data = inseries.values
    ''''\n    Implementation of the CUSUM filter, where E_{t − 1}[y_t] = y_{t − 1}\n    :param inseries: the raw time series we wish to filter\n    :param posThreshold:  the threshold for positive deltas in CUSUM\n    :param negThreshold:  the threshold for negative deltas in CUSUM; if None, then posThreshold is used\n    :param applyDiff: if True, apply diff() (diffs of consecutive values) to the input series\n    :return: a tuple (posEventIndex, negEventIndex) with subset of inseries.index elements,\n            for each position where:\n                posEventIndex: CUSUM-value was over the positive threshold (i.e. triggered an "alarm")\n                negEventIndex: CUSUM-value was over the negative threshold\n    '''
    negThreshold = (negThreshold if (negThreshold is not None) else posThreshold)
    cdef float posSum
    cdef float negSum
    (posEventsList, negEventsList) = ([], [])
    (posSum, negSum) = (0.0, 0.0)
    # next we need to get rid off these very slow pandas operations
    # this can easily be done as numpy provides the very same features
    '''diff = (inseries.diff() if applyDiff else inseries)'''
    diff = (np.diff(data) if applyDiff else data)
    for i in range(1, len(diff)):
    # so the ilocs need to be replaced
        '''
        cdef float posSum = max(0, (posSum + diff.iloc[i]))
        cdef float negSum = min(0, (negSum + diff.iloc[i]))'''
        posSum = max(0, (posSum + diff[i]))
        negSum = min(0, (negSum + diff[i]))
        if (negSum < (- negThreshold)):
            negSum = 0
            negEventsList.append(i)
        elif (posSum > posThreshold):
            posSum = 0
            posEventsList.append(i)
    # as we are still using pandas here, we keep this for now
    negEventIndex = inseries.index[negEventsList]
    posEventIndex = inseries.index[posEventsList]
    return (posEventIndex, negEventIndex, posEventsList, negEventsList)


#so let's try this again, but this time as an actual C implementation

cpdef get_CUSUM_v01_np_outside(inseries: pd.Series.values, posThreshold, negThreshold=None, applyDiff=True):
    '''\n    Implementation of the CUSUM filter, where E_{t − 1}[y_t] = y_{t − 1}\n    :param inseries: the raw time series we wish to filter\n    :param posThreshold:  the threshold for positive deltas in CUSUM\n    :param negThreshold:  the threshold for negative deltas in CUSUM; if None, then posThreshold is used\n    :param applyDiff: if True, apply diff() (diffs of consecutive values) to the input series\n    :return: a tuple (posEventIndex, negEventIndex) with subset of inseries.index elements,\n            for each position where:\n                posEventIndex: CUSUM-value was over the positive threshold (i.e. triggered an "alarm")\n                negEventIndex: CUSUM-value was over the negative threshold\n    '''
    negThreshold = (negThreshold if (negThreshold is not None) else posThreshold)
    cdef float posSum
    cdef float negSum
    (posEventsList, negEventsList) = ([], [])
    (posSum, negSum) = (0.0, 0.0)
    # next we need to get rid off these very slow pandas operations
    # this can easily be done as numpy provides the very same features
    '''diff = (inseries.diff() if applyDiff else inseries)'''
    diff = (np.diff(inseries) if applyDiff else inseries)
    for i in range(1, len(diff)):
    # so the ilocs need to be replaced
        '''
        cdef float posSum = max(0, (posSum + diff.iloc[i]))
        cdef float negSum = min(0, (negSum + diff.iloc[i]))'''
        posSum = max(0, (posSum + diff[i]))
        negSum = min(0, (negSum + diff[i]))
        # this could also need some optimization but we'll keep this for now
        if (negSum < (- negThreshold)):
            negSum = 0
            negEventsList.append(i)
        elif (posSum > posThreshold):
            posSum = 0
            posEventsList.append(i)
    # and finally get rid off those last pandas operations
    # so let's make this arrays
    '''
    negEventIndex = inseries.index[negEventsList]
    posEventIndex = inseries.index[posEventsList]
    '''
    negEventIndex = np.array(negEventsList)
    posEventIndex = np.array(posEventsList)
    return (posEventIndex, negEventIndex, posEventsList, negEventsList)


#but we also want memoryviews in our function to make actually really fast

def get_CUSUM_v01_np_inside_mem(inseries: pd.Series, posThreshold, negThreshold=None, applyDiff=True):
    #extract data from pandas series
    data = inseries.values
    #so we cast our nd
    cdef int[:] arr = data
    cdef float posSum
    cdef float negSum
    ''''\n    Implementation of the CUSUM filter, where E_{t − 1}[y_t] = y_{t − 1}\n    :param inseries: the raw time series we wish to filter\n    :param posThreshold:  the threshold for positive deltas in CUSUM\n    :param negThreshold:  the threshold for negative deltas in CUSUM; if None, then posThreshold is used\n    :param applyDiff: if True, apply diff() (diffs of consecutive values) to the input series\n    :return: a tuple (posEventIndex, negEventIndex) with subset of inseries.index elements,\n            for each position where:\n                posEventIndex: CUSUM-value was over the positive threshold (i.e. triggered an "alarm")\n                negEventIndex: CUSUM-value was over the negative threshold\n    '''
    negThreshold = (negThreshold if (negThreshold is not None) else posThreshold)
    (posEventsList, negEventsList) = ([], [])
    (posSum, negSum) = (0.0, 0.0)
    # next we need to get rid off these very slow pandas operations
    # this can easily be done as numpy provides the very same features
    '''diff = (inseries.diff() if applyDiff else inseries)'''
    diff = (helpers.diff(data) if applyDiff else data)
    for i in range(1, len(diff)):
    # so the ilocs need to be replaced
        '''
        cdef float posSum = max(0, (posSum + diff.iloc[i]))
        cdef float negSum = min(0, (negSum + diff.iloc[i]))'''
        posSum = max(0, (posSum + diff[i]))
        negSum = min(0, (negSum + diff[i]))
        if (negSum < (- negThreshold)):
            negSum = 0
            negEventsList.append(i)
        elif (posSum > posThreshold):
            posSum = 0
            posEventsList.append(i)
    # and finally get rid off those last pandas operations
    '''
    negEventIndex = inseries.index[negEventsList]
    posEventIndex = inseries.index[posEventsList]
    '''
    #not like this
    negEventIndex = np.array(negEventsList)
    posEventIndex = np.array(posEventsList)
    return (posEventIndex, negEventIndex, posEventsList, negEventsList)

# further we want this to be a C function..

cpdef get_CUSUM_v01_np_outside_mem(inseries: pd.Series.values, posThreshold, negThreshold=None, applyDiff=True):
    ''''\n    Implementation of the CUSUM filter, where E_{t − 1}[y_t] = y_{t − 1}\n    :param inseries: the raw time series we wish to filter\n    :param posThreshold:  the threshold for positive deltas in CUSUM\n    :param negThreshold:  the threshold for negative deltas in CUSUM; if None, then posThreshold is used\n    :param applyDiff: if True, apply diff() (diffs of consecutive values) to the input series\n    :return: a tuple (posEventIndex, negEventIndex) with subset of inseries.index elements,\n            for each position where:\n                posEventIndex: CUSUM-value was over the positive threshold (i.e. triggered an "alarm")\n                negEventIndex: CUSUM-value was over the negative threshold\n    '''
    #so we make the array a memview again
    cdef int[:] arr = inseries
    negThreshold = (negThreshold if (negThreshold is not None) else posThreshold)
    cdef float posSum
    cdef float negSum
    (posEventsList, negEventsList) = ([], [])
    (posSum, negSum) = (0.0, 0.0)
    # next we need to get rid off these very slow pandas operations
    # this can easily be done as numpy provides the very same features
    '''diff = (inseries.diff() if applyDiff else inseries)'''
    diff = (helpers.diff(inseries) if applyDiff else inseries)
    for i in range(1, len(diff)):
    # so the ilocs need to be replaced
        '''
        cdef float posSum = max(0, (posSum + diff.iloc[i]))
        cdef float negSum = min(0, (negSum + diff.iloc[i]))'''
        posSum = max(0, (posSum + diff[i]))
        negSum = min(0, (negSum + diff[i]))
        if (negSum < (- negThreshold)):
            negSum = 0
            negEventsList.append(i)
        elif (posSum > posThreshold):
            posSum = 0
            posEventsList.append(i)
    # and finally get rid off those last pandas operations
    negEventIndex = np.array(negEventsList)
    posEventIndex = np.array(posEventsList)
    return (posEventIndex, negEventIndex, posEventsList, negEventsList)
