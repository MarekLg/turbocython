# TurboCython
# (C) Roman Pulgrabja, Marek Legris, 2019

import inspect
import os
import typed_ast.ast3 as ast
import astunparse
import pyxUnparsing


def parse_pyx(src):
    """Converts python source code to cython .pyx source code

    Args:
        src: A string containing python source code

    Returns:
        A string containing cython .pyx source code
    """
    pyxSrc = "import cython\nimport numpy"
    tree = ast.parse(src)

    packagePath = os.path.dirname(__file__)
    unparsedTree = pyxUnparsing.Unparse(tree, packagePath)
    pyxSrc += unparsedTree.src
    
    return pyxSrc