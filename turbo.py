# TurboCython - transforms python code to cython optimized module
# (C) Roman Pulgrabja, Marek Legris, 2019

import os
import inspect
import sys
import parsing

def _create_pyx_path(srcPath):
    baseNoExtension = os.path.splitext(os.path.basename(srcPath))[0]
    dirpath = os.path.dirname(srcPath)
    newPath = os.path.join(dirpath, baseNoExtension + ".pyx")
    return newPath


def _create_setup_path(srcPath):
    dirpath = os.path.dirname(srcPath)
    newPath = os.path.join(dirpath, "setup.py")
    return newPath


def _write_file(path, src):
    with open(path, "w") as srcFile:
        srcFile.write(src)
        srcFile.close()


def _get_src(srcFilePath):
    srcFile = open(srcFilePath, "r")
    src = srcFile.read()
    srcFile.close()
    return src


def create_pyx(srcFilePath):
    """Creates a .pyx file based on existing .py sourcecode

    Args:
        srcFilePath: Path to the .py sourcecode file

    Returns:
        The path to the newly created .pyx file
    """
    src = _get_src(srcFilePath)

    pyxSrc = parsing.parse_pyx(src)

    pyxFilePath = _create_pyx_path(srcFilePath)

    _write_file(pyxFilePath, pyxSrc)
    print ("Saving cython declaration file: ", pyxFilePath)
    return pyxFilePath


def create_setup(srcFilePath):
    """Creates a interpretable cython setup file based on existing .py sourcecode

    Args:
        srcFilePath: Path to the .py sourcecode file

    Returns:
        The path to the newly created setup file
    """
    setupSrc = """
from distutils.core import setup
from Cython.Build import cythonize
import numpy
import pandas

setup(
    ext_modules = cythonize(["{0}"]),
)""".format(srcFilePath)

    setupFilePath = _create_setup_path(srcFilePath)
    _write_file(setupFilePath, setupSrc)
    print("Saving cython setup file:", setupFilePath)

    return setupFilePath


def main():
    srcFilePath = sys.argv[1]

    pyxFilePath = create_pyx(srcFilePath)
    setupFilePath = create_setup(pyxFilePath)

    print("Executing cython setup...")
    os.system("python3 {0} build_ext --inplace".format(setupFilePath))

if __name__ == "__main__":
    main()