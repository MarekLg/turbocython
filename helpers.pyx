import cython
import numpy as np
#cimport numpy as np


def containsMember(member):
    memberFnc = ['diff', 'iloc', 'index']
    isMember = False
    for i in memberFnc:
        if i == member:
            isMember = True
    return isMember

def containsMember2(member):
    whiteList = ['iloc', 'index']

    return member in locals() or member in whiteList

def containsMember3(member):
    try:
        f = locals()[member]
        f()
        return True
    except AttributeError:
        return False
    except TypeError:
        pass

# derived from numpy/function_base/diff()
cpdef diff(double[:] memview, int n = 1, int axis = -1):
    cdef double[:] dff = np.diff(memview, n, axis)
    return dff
    # if n == 0:
    #     return memview
    # if n < 0:
    #     raise ValueError(
    #         "order must be non-negative but got ")
    # nd = len(memview)
    # # for now we will handle this with numpy.ndarrays but this really should be handled with memoryviews
    # slice1=[np.ndarray(shape=(3), buffer=np.array([np.nan, np.nan, np.nan]), dtype='float')]*nd
    # slice2=[np.ndarray(shape=(3), buffer=np.array([np.nan, np.nan, np.nan]), dtype='float')]*nd
    # slice1[axis]=np.array([1, np.nan, np.nan])
    # slice1[axis]=np.array([np.nan, -1, np.nan])
    # #this needs to be implemented in cython style
    # # on second thought this is probably not needed, as numpy array seem to be
    # # indexable with other numpy arrays so this is most likely obsolete
    # #
    # # slice1 = tuple(slice1)
    # # slice2 = tuple(slice2)
    #
    # #this needs some revision
    # # hope this does the trick
    # cdef int[:] tryAgain = np.asarray(memview[slice1])-np.asarray(memview[slice2])
    # if n > 1:
    #     return diff(tryAgain, n-1, axis=axis)
    #     #return diff(slice1-slice2, n-1, axis=axis)
    # else:
    #     return tryAgain
        #return np.asarray(memview[slice1]-memview[slice2])
        #return slice1-slice2
